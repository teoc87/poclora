const express = require('express');
const mycontroller = require('./mycontroller');

const router = express();

router.post('/api/prova', mycontroller.formatta);
router.get('/api/mostra', mycontroller.mostra);

module.exports = router;
