const mongoose = require('mongoose');
mongoose.Promise = require('bluebird');
mongoose.set('useFindAndModify', false);
const Schema = mongoose.Schema;

const SensoreSchema = new Schema({
  temperatura: {
    type: Number,
    required: false
  },
  umidita: {
    type: Number,
    required: false
  },
  mdate: {
    type: Date,
    default: Date.now
    }
});

module.exports = mongoose.model('Sensore', SensoreSchema, 'sensore');